/** @jsx h */
import { component, h } from 'literaljs';
import './index.scss';

import Navbar from '../Navbar';

export default component({
	methods() {
		return {
			toTop() {
				return scroll(0, 0);
			}
		};
	},
	render() {
		return (
			<div class="Content">
				<Navbar />
				<div class="right">
					<h1 id="introduction">Introduction</h1>
					<p>
						LiteralJS is a JavaScript library for building user
						interfaces.
					</p>
					{
						<pre class="language-js">
							{/* prettier-ignore */}
							<code>
								{`Build "literaljs" to build:
       1856 B: index.js.gz
       1611 B: index.js.br
       1859 B: index.m.js.gz
       1620 B: index.m.js.br
       1919 B: index.umd.js.gz
       1680 B: index.umd.js.br`}
              </code>
						</pre>
					}
					<h2 id="features">Features</h2>
					<ul>
						<li>
							<span class="title">Small:</span> Under{' '}
							<code>2kb</code> in size (using microbundle).
						</li>
						<li>
							<span class="title">Startup:</span>
							Amazing startup metrics; Great for weaker devices
							and weaker connections.
						</li>
						<li>
							<span class="title">Simple:</span>
							LiteralJS uses regular functions and applies
							structure to building components for easier
							development.
						</li>
						<li>
							<span class="title">Fast:</span> Current and
							previous vDOM data is diffed instead of the actual
							DOM for performant updates and rendering.
						</li>
						<li>
							<span class="title">Virtual DOM:</span> Diffing
							occurs only on state update for more efficient DOM
							updates.
						</li>
						<li>
							<span class="title">Flexible Syntax:</span> Freedom
							to use JSX, Hyperscript, or Object syntax.
						</li>
						<li>
							<span class="title">Lifecycle Methods:</span> All
							components have the ability to trigger mounted,
							updated, and unmounted lifecycle methods.
						</li>
						<li>
							<span class="title">Global Application State:</span>{' '}
							One source of truth which makes other state
							management libraries (like Redux) less of a need.
						</li>
						<li>
							<span class="title">Component State:</span> Manage
							state individually per component.
						</li>
						<li>
							<span class="title">Event Delegation:</span> Uses
							event delegation for most events in your application
							for increased performance.
						</li>
					</ul>
					<h2 id="getting-started">Getting Started</h2>
					<p>
						The easiest way of getting started is by cloning the{' '}
						<a href="https://gitlab.com/mikeysax/LiteralJS-Starter">
							LiteralJS Starter Application
						</a>
						.
					</p>
					<br />
					<p>
						Once you clone the project, you <code>cd</code> to the
						directory and run the following commands:
					</p>
					<pre class="language-js">
						<code>{`$ npm install
$ npm run dev
// OR
$ yarn
$ yarn dev`}</code>
					</pre>
					<p>
						This command is setup to launch a browser window and you
						should see a landing page with the LiteralJS logo.
					</p>

					<h2 id="installation">Installation</h2>
					<p>
						To install <code>literaljs</code> using <code>npm</code>{' '}
						you can type the following in the terminal:
					</p>
					<pre class="language-js">
						<code>{`$ npm install literaljs`}</code>
					</pre>
					<br />
					<p>
						To install <code>literaljs</code> using{' '}
						<code>yarn</code> you can type the following in the
						terminal:
					</p>
					<pre class="language-js">
						<code>{`$ yarn add literaljs`}</code>
					</pre>
					<p>
						Once <code>literaljs</code> is installed you can{' '}
						<code>import</code> or <code>require</code> it into your
						project:
					</p>
					<pre class="language-jsx">
						<code>{`const Literal = require('literaljs');
// OR
import Literal from 'literaljs';
// OR
import { component, render, h } from 'literaljs';`}</code>
					</pre>
					<h2 id="example-apps">Example Applications</h2>
					<ul>
						<li>
							<span class="title">Todo Application</span>:{' '}
							<a href="https://literaljs-todo.netlify.com/">
								https://literaljs-todo.netlify.com/
							</a>
						</li>
					</ul>
					<h2 id="markup-syntax">Markup Syntax</h2>
					<p>
						With <code>literaljs</code>, you can use three different
						types of markup syntax:
					</p>
					<h3>JSX</h3>
					<a href="#using-jsx">How do I use JSX in LiteralJS?</a>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  render() {
    const { count } = this.state;
    return (
      <div class='container'>
        This is the Count: {count}
        <button events={{ click: () => this.setStore(({ count }) => ({ count: count + 1 })) }}>
          Click Me!
        </button>
      </div>
    );
  }
});

render(foo, 'app', { count: 1 });`}</code>
					</pre>
					<h3>Hyper Script</h3>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  render() {
    const { count } = this.state;
    return h('div', {
        class: 'container'
      },
      \`This is the Count: \${count}\`,
      h('button', {
        events: { click: () => this.setStore(({ count }) => ({ count: count + 1 })) }
      })
    );
  }
});

render(foo, 'app', { count: 1 });`}</code>
					</pre>
					<h3>Object</h3>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  render() {
    const { count } = this.state;
    return ({
      el: 'div',
      attr: {
        class: 'container'
      },
      children: [
        \`This is the Count: \${count}\`,
        {
          el: 'button',
          attr: {
            events: { click: () => this.setStore(({ count }) => ({ count: count + 1 })) }
          }
        },
        'Click Me!'
      ]
    });
  }
});

render(foo, 'app', { count: 1 });`}</code>
					</pre>
					<h3 id="using-jsx">Using JSX With LiteralJS</h3>
					<p>
						If you want to use JSX, you need to import the{' '}
						<code>Literal.h</code> function, install the{' '}
						<a href="https://babeljs.io/docs/en/babel-plugin-transform-react-jsx">
							JSX transform plugin
						</a>{' '}
						, and add the pragma option to your{' '}
						<code>.babelrc</code> or <code>package.json</code> file.
					</p>
					<h4>
						In <code>.babelrc</code>:
					</h4>
					<pre class="language-jsx">
						<code>{`{
  "plugins": ["@babel/plugin-transform-react-jsx"]
}`}</code>
					</pre>
					<p>
						Any file which contains JSX will also need the following
						at the top:
					</p>
					<pre class="language-jsx">
						<code>{`/** @jsx h */`}</code>
					</pre>
					<h4>Considerations On JSX In LiteralJS</h4>
					<p>
						JSX in LiteralJS has some differentiation when it comes
						to other implementations like JSX in React.
					</p>
					<br />
					<p>
						The biggest differentiator in syntax from React JSX is
						that the <code>class</code>
						attribute is acceptable on JSX elements,{' '}
						<code>props</code> are passed to components as an
						object, and <code>events</code> are passed as an object.
					</p>
					<br />
					<p>
						Event Objects need a key (which is a DOM event name) and
						a function as a value. Also, any name is considered
						acceptable as an attribute and any keys that work with
						LiteralJS Object Markup Syntax also work in LiteralJS
						JSX markup.
					</p>

					<h2 id="render-function">Render Function</h2>
					<p>
						The <code>Literal.render</code> function is responsible
						for parsing the AST that is provided with{' '}
						<code>component</code>s, injecting the generated markup
						into the specific DOM node, diffing any new changes
						based on store or state changes, and setting the default
						application store.
					</p>
					<br />
					<p>
						The <code>Literal.render</code> function can accept 3
						arguments, one of which is optional.
					</p>
					<div class="list">
						<div>
							1) The root <code>component</code>.
						</div>
						<div>
							2) String <code>id</code> of the DOM node to inject
							generated markup into.
						</div>
						<div>
							3) (Optional) The default store (global application
							state) as an Object.
						</div>
					</div>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`Literal.render(component, 'app', { count: 0 });
// or
render(component, 'app');`}</code>
					</pre>
					<h2 id="component-function">Component Function</h2>
					<p>
						The <code>component</code> function accepts objects with
						the following keys:
					</p>
					<div class="list">
						<a href="#component-props">props</a>
						<a href="#component-methods">methods</a>
						<a href="#component-render">render</a>
						<a href="#component-getState">getState</a>
						<a href="#component-setState">setState</a>
						<a href="#component-getStore">getStore</a>
						<a href="#component-setStore">setStore</a>
						<a href="#component-lifecycle">lifecycle methods</a>
						<a href="#component-iterate">Iterating Components</a>
					</div>
					<h3 id="component-props">props</h3>
					<p>
						The <code>props</code> of a <code>component</code> is an
						object which can be passed with anything that is
						normally valid in a JavaScript Object. This can include
						any data or functions.
					</p>
					<br />
					<p>
						The only <code>component</code> that can not recieve{' '}
						<code>props</code> is the root <code>component</code>
						that is passed to the <code>Literal.render</code>{' '}
						function.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Bar = component({
  render() {
    const { total } = this.props;
    return <p>Total: {total}</p>
  }
});

const Foo = component({
  render() {
    return <div>
      <Bar props={{ total: 9 }} />
    </div>
  }
});`}</code>
					</pre>
					<h3 id="component-methods">methods</h3>
					<p>
						The <code>methods</code> key of a <code>component</code>{' '}
						will accept a function which returns an object of other
						functions. Within the <code>methods</code>, you have
						access to
						<code>this.getStore</code>, <code>this.setStore</code>,
						<code>this.getState</code>, and{' '}
						<code>this.setState</code>, functions which can be used
						accordingly. By defining <code>methods</code>, you can
						use them directly within the <code>component</code>s{' '}
						<code>render</code> function or within other methods by
						referencing the method name in <code>this</code>.
					</p>
					<p class="-important">
						Methods and a method should never be an arrow function.
					</p>
					<h3 id="component-getState">getState</h3>
					<p>
						<code>getState</code> is a function which is accessible
						anywhere in a component by referencing{' '}
						<code>this.getState</code>.
					</p>
					<br />
					<p>
						Using the <code>getState</code> function will pull a
						copy of the latest component state object. Once used,
						the specific state keys are accessible via dot notation
						or object notation.
					</p>
					<h3 id="component-setState">setState</h3>
					<p>
						The <code>setState</code> function accepts an object or
						a function which returns an object. Using a function
						also gives you access to the most current state as an
						argument of the function. When calling{' '}
						<code>setState</code>, you can functionally modify the
						current component state by key value.
						<code>setState</code> is accessible anywhere in a
						component by referencing <code>this.setState</code>
						<code>methods</code> function.
					</p>
					<br />
					<p>
						Using the <code>setState</code> function will cause the
						component state to update and diffing to occur.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`component({
  state: {
	  count: 0
  },
  methods() {
    return {
      increase() {
        return this.setState(({ count }) => ({ count: count + 1 }));
      }
    };
  },
  render() {
    const { count } = this.getState();
    return (
      <button events={{ click: this.increase }}>
        Current Count: {count}
      </button>
    );
  }
});

Literal.render(component, 'app', {});`}</code>
					</pre>
					<h3 id="component-getStore">getStore</h3>
					<p>
						<code>getStore</code> is a function which is accessible
						anywhere in a component by referencing{' '}
						<code>this.getStore</code>.
					</p>
					<br />
					<p>
						Using the <code>getStore</code> function will pull a
						copy of the latest state object of your application.
						Once used, the specific state keys are accessible via
						dot notation or object notation.
					</p>
					<h3 id="component-setStore">setStore</h3>
					<p>
						The <code>setStore</code> function accepts an object or
						a function which returns an object. Using a function
						also gives you access to the most current store as an
						argument of the function. When calling{' '}
						<code>setStore</code>, you can functionally modify the
						current global application store by key value.
						<code>setStore</code> is accessible anywhere in a
						component by referencing <code>this.setStore</code>
						<code>methods</code> function.
					</p>
					<br />
					<p>
						Using the <code>setStore</code> function will cause the
						application store to update and diffing to occur.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`component({
  methods() {
    const { count } = this.getStore();
    return {
      increase() {
        return this.setStore({ count: count + 1 });
      }
    };
  },
  render() {
    const { count } = this.getStore();
    return (
      <button events={{ click: this.increase }}>
        Current Count: {count}
      </button>
    );
  }
});

Literal.render(component, 'app', { count: 0 });`}</code>
					</pre>
					<h4 id="component-lifecycle">lifecycle methods</h4>
					<p>
						Similar to other front-end frameworks, there are three
						lifecycle events that LiteralJS includes that can be
						attached to any component.
					</p>
					<p>
						To do so, just define a method call{' '}
						<code>unmounted</code>, <code>updated</code>, or{' '}
						<code>mounted</code> in a component to trigger functions
						when a component is first placed in the DOM, updated, or
						removed from the DOM.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`component({
	mounted() {
		console.log('I am on the page!');
	},
	updated() {
		console.log('I am updated!');
	},
	unmounted() {
		console.log('I am off the page!');
	},
  render() {
    return (
      <div>
        I have a life!
      </div>
    );
  }
});`}</code>
					</pre>
					<h4 id="component-iterate">Iterating Components</h4>
					<p>
						Similar to other front-end frameworks, when iterating
						over a component, we need to supply a unique key. The
						key should be a unique and not change, like an id from a
						database, and not be the index of the iteration. The key
						is used in generation of a component state object and is
						used in tracking the individual component state object.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`var TodoCard = component({
  render() {
    const { text } = this.props;
    return (
	  <div>
		{text}
	  </div>
	)
  }
});

var App = component({
  state: {
	  todos: []
  },
  render() {
    return (
      <div>
        {todos.map((todo, index) => (
		  <TodoCard
		    props={{
			  key: todo.id,
			  text: todo.text,
			}}
		  />
		))}
      </div>
    );
  }
});`}</code>
					</pre>
					<h3 id="component-render">render</h3>
					<p>
						The <code>render</code> key of a <code>component</code>{' '}
						accepts a function that returns an Object. The{' '}
						<code>render</code> function of a <code>component</code>{' '}
						is where all markup (like JSX) should be placed, events
						and methods used, and be primarily used to organize any
						other visual aspects of your application.
					</p>
					<p class="-important">
						Render should never be an arrow function but events can
						use arrow functions
					</p>
					<br />
					<p>
						You have access to several functions and objects within
						the properties of the <code>render</code> key:
					</p>
					<div class="list">
						<a href="#render-state">state</a>
						<a href="#render-getState">getState</a>
						<a href="#render-setState">setState</a>
						<a href="#render-getStore">getStore</a>
						<a href="#render-setStore">setStore</a>
						<a href="#render-methods">methods</a>
						<a href="#render-props">props</a>
						<a href="#render-events">events</a>
					</div>
					<h4 id="render-state">state (Object)</h4>
					<p>
						The <code>state</code> object is defined directly on a
						component. This is accessible anywhere in the component
						by referencing <code>this.getState</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  state: {
    count: 1
  },
  render() {
    const { count } = this.getState();
    return (
      <button events={{
        click: () => console.log(count)
      }}>
        Current Count: {count}
      </button>
    );
  }
});

render(Foo, 'app', {});`}</code>
					</pre>
					<h4 id="render-getState">getState (Function -> Object)</h4>
					<p>
						The <code>getState</code> function returns a new object
						of the current component state. This function is
						accessible anywhere in the component by referencing{' '}
						<code>this.getState</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  state: {
    count: 1
  },
  render() {
    const { count } = this.getState();
    return (
      <button events={{
        click: () => console.log(count)
      }}>
        Current Count: {count}
      </button>
    );
  }
});

render(Foo, 'app', {});`}</code>
					</pre>
					<h4 id="render-setState">setState (Function)</h4>
					<p>
						The <code>setState</code> function accepts an object or
						a function which returns an object. Using a function
						also gives you access to the most current state as an
						argument of the function. When calling{' '}
						<code>setState</code>, you can functionally modify the
						current component state by key value. This function is
						accessible anywhere in a component by referencing{' '}
						<code>this.setState</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
	state: {
		count: 1
	},
	render() {
		const { count } = this.getState();
		return (
			<div>
				<p>Current Count: {count}</p>
				<button events={{
					click: () => this.setState(({ count }) => ({ count: count + 1 }))
				}}>
					Add One To Count
				</button>
				<button events={{
					click: () =>
						this.setState(
							(state) => ({ count: state.count - 1 }),
							() => console.log('Callback: Minus 1 From Count')
						)
				}}>
					Minus Count
				</button>
			</div>
		);
	}
});

render(Foo, 'app', {});`}</code>
					</pre>
					<h4 id="render-getStore">getStore (Function -> Object)</h4>
					<p>
						The <code>getStore</code> function returns a new object
						of the current global application store. This function
						is accessible anywhere in the component by referencing{' '}
						<code>this.getStore</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  render() {
    const { count } = this.getStore();
    return (
      <button events={{
        click: () => console.log(count)
      }}>
        Current Count: {count}
      </button>
    );
  }
});

render(Foo, 'app', { count: 1 });`}</code>
					</pre>
					<h4 id="render-setStore">setStore (Function)</h4>
					<p>
						The <code>setStore</code> function accepts an object or
						a function which returns an object. Using a function
						also gives you access to the most current store as an
						argument of the function. When calling{' '}
						<code>setStore</code>, you can functionally modify the
						current global application store by key value. This
						function is accessible anywhere in a component by
						referencing <code>this.setStore</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
	render() {
		const { count } = this.getStore();
		return (
			<div>
				<p>Current Count: {count}</p>
				<button events={{
					click: () => this.setStore({ count: count + 1 })
				}}>
					Add One To Count
				</button>
				<button events={{
					click: () =>
						this.setStore(
							(state) => ({ count: state.count - 1 }),
							() => console.log('Callback: Minus 1 From Count')
						)
				}}>
					Minus Count
				</button>
			</div>
		);
	}
});

render(Foo, 'app', { count: 1 });`}</code>
					</pre>
					<h4 id="render-methods">
						methods (Function -> Object -> Functions)
					</h4>
					<p>
						The <code>methods</code> property is a function which
						returns an object of functions which are defined on the{' '}
						<code>component</code> instance.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  methods() {
    const { count } = this.getStore();
    return {
      increase() {
        return this.setStore({ count: count + 1 });
      }
    }
  },
  render() {
    const { count } = this.getStore();
    return (
      <button events={{ click: this.increase }}>
        Current Count: {count}
      </button>
    );
  }
});`}</code>
					</pre>
					<p class="-important">
						<strong>Important:</strong> Methods can be used to clean
						up markup and organize logic.
					</p>
					<h4 id="render-props">props (Object, Function)</h4>
					<p>
						The <code>props</code> property is an object or function
						which is defined when the <code>component</code> is
						instantiated within another <code>component</code>.
					</p>
					<h4>Examples:</h4>
					<pre class="language-jsx">
						<code>{`const Bar = component({
  render() {
    const { total } = this.props;
    return <p>Total: {total}</p>
  }
});

const Foo = component({
  render() {
    return (
		<div>
			<Bar props={{ total: 9 }} />
		</div>
	);
  }
});`}</code>
					</pre>
					<p class="-important">
						<strong>Important:</strong> You can use other popular
						patters like render props or HoC in a similar way to how
						React using this process.
					</p>
					<h4 id="render-events">events (Object)</h4>
					<p>
						The <code>events</code> key in markup accepts an Object.
					</p>
					<p>
						An event object consists of the <code>key</code> of the
						event name and the <code>value</code> of a function to
						trigger:
					</p>
					<ul>
						<li>Key: Event Name</li>
						<li>Value: Function To Trigger</li>
					</ul>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Foo = component({
  render() {
    return (
      <button events={{ click: () => console.log('Hello World!') }}>
        Click Me!
      </button>
    );
  }
});`}</code>
					</pre>
					<h2 id="literaljs-router">LiteralJS-Router</h2>
					<p>
						LiteralJS-Router is a small library that can be used to
						add routing to your application in a similar manner to
						other routing library found in other frameworks, like
						React Router.
					</p>
					<ul>
						<li>
							<span class="title">
								LiteralJS-Router Gitlab Repository
							</span>
							:{' '}
							<a href="https://gitlab.com/mikeysax/literaljs-router">
								https://gitlab.com/mikeysax/literaljs-router
							</a>
						</li>
					</ul>
					<ul>
						<li>
							<span class="title">LiteralJS-Router NPM</span>:{' '}
							<a href="https://www.npmjs.com/package/literaljs-router">
								https://www.npmjs.com/package/literaljs-router
							</a>
						</li>
					</ul>
					<h2 id="installation">Installation</h2>
					<p>
						To install <code>literaljs-router</code> using{' '}
						<code>npm</code> you can type the following in the
						terminal:
					</p>
					<pre class="language-js">
						<code>{`$ npm install literaljs-router`}</code>
					</pre>
					<br />
					<p>
						To install <code>literaljs-router</code> using{' '}
						<code>yarn</code> you can type the following in the
						terminal:
					</p>
					<pre class="language-js">
						<code>{`$ yarn add literaljs-router`}</code>
					</pre>
					<p>
						Once <code>literaljs-router</code> is installed you can{' '}
						<code>import</code> or <code>require</code> it into your
						project:
					</p>
					<pre class="language-jsx">
						<code>{`import { Router } from 'literaljs-router';`}</code>
					</pre>
					<h3 id="router">Router</h3>
					<p>
						The <code>Router</code> component contains the logic to
						display specific components based on the associated
						paths and requires <code>routes</code>,{' '}
						<code>path</code>, the store <code>key</code> name, and{' '}
						<code>set</code> which is the coresponding{' '}
						<code>getState</code> method to be passed to it as{' '}
						<code>props</code>.
					</p>
					<p>
						The <code>routes</code> that are passed to the{' '}
						<code>Router</code> as <code>props</code> is an array
						with multiple objects. These route objects should
						contain a <code>path</code> (string) and{' '}
						<code>render</code> (component) as demonstrated in the
						following example.
					</p>
					<p>
						The <code>path</code> that is passed to the{' '}
						<code>Router</code> as <code>props</code> is matched
						against the current browser pathname and should come
						from the application store. This is used to change which
						associated component the <code>Router</code> displays
						when the path changes.
					</p>
					<p class="-important">
						<strong>Important:</strong> The <code>path</code> has
						the option of being a string which is converted into a
						regular expression. A regular expression can also be
						used. Regular expressions should be primarily used when
						pieces of the path need to be dynamic. Dynamic parts of
						a path include things like ids and other meta data not
						included as query params of the URL.
					</p>
					<p>
						The <code>key</code> props is a string which is the name
						of the application store key-value that is used for
						switching the current browser pathname.
					</p>
					<p>
						The <code>set</code> prop is the corresponding{' '}
						<code>setStore</code> method. This is determined by
						where and how you wish to change the current route.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`
import { Router } from 'literaljs-router';

// Components
import Navbar from './components/Navbar';
import Flash from './components/Flash';
import Footer from './components/Footer';

// Pages
import IndexPage from '../pages/index';
import SignInPage from '../pages/sign-in';
import SignUpPage from '../pages/sign-up';
import ProfilePage from '../pages/profile';
import NotFoundPage from '../pages/notFound';

// Pages
const routes = [
	{
		path: '/',
		render: IndexPage
	},
	{
		path: '/sign-in',
		render: SignInPage
	},
	{
		path: '/sign-up',
		render: SignUpPage
	},
	{
		path: /\\/profile\\/[a-z|\\d]/, // This regular expression is used to match profile ids.
		render: ProfilePage
	},
	{
		path: '404', // Use this to display a component for an unmatched path
		render: NotFoundPage
	}
];

const App = component({
	render() {
		const { location } = this.state;
		return (
			<div class="App">
				<Navbar />
				<Flash />
				<Router
					props={{
						path: location,
						routes: routes,
						key: 'location',
						set: this.setStore
					}}
				/>
				<Footer />
			</div>
		);
	}
});

render(App, 'app', { location: location.pathname });`}</code>
					</pre>
					<p>
						With the <code>path</code> located in the application
						state, you can change what the <code>Router</code>{' '}
						displays by changing that specific key value using the
						corresponding <code>setState</code>.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`const Navbar = component({
	methods() {
		return {
			goToSignUp() {
				this.setStore({ location: '/sign-up' });
			}
		}
	},
	render() {
		return (
			<div class="Navbar">
				<button events={{ click: this.goToSignUp }}>
					Sign Up
				</button>
			</div>
		);
	}
});`}</code>
					</pre>
					<h3 id="getParams">getParams (Function -> Object)</h3>
					<p>
						The <code>getParams</code> function can be used to
						easily grab the current URL query params or search
						parameters as an object.
					</p>
					<h4>Example:</h4>
					<pre class="language-jsx">
						<code>{`import { getParams } from 'literaljs-router';`}</code>
					</pre>
					<h2 id="deployment">Deployment</h2>
					<p>
						Deploying a LiteralJS application is very straight
						forward!
					</p>
					<h3 id="deployment-netlify">Netlify</h3>
					<p>
						By default, the LiteralJS Starter application is setup
						to easily deploy to Netlify.
					</p>
					<p>
						The only thing needed is to setup the build command in
						the Netlify dashboard to <code>build-netlify</code>, a{' '}
						<code>.nvmrc</code> file with the latest node version,
						and the <strong>Publish directory</strong> set to{' '}
						<code>dist</code>:
					</p>
					<p>
						<strong>Build command:</strong>{' '}
						<code>npm run build-netlify</code>
					</p>
					<p>
						<strong>Publish directory:</strong> <code>dist</code>
					</p>
					<div
						class="to-top"
						events={{ click: this.toTop }}
						title="Back To Top"
					>
						^
					</div>
				</div>
			</div>
		);
	}
});
