.Navbar {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  flex-wrap: nowrap;
  width: 200px;
  background-color: white;
  border: 1px solid #cccccc;
  border-radius: 3px;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.05);
}
@media only screen and (max-width: 1100px) {
  .Navbar {
    width: 100%;
  }
}
.Navbar > .title {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  width: 100%;
}
.Navbar > .links {
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  background-color: white;
  border-top: 1px solid #cccccc;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.05);
  width: 100%;
  height: 50px;
  padding: 0;
}
.Navbar > .links > a {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  color: #333;
  transition: all 0.1s ease-in;
  font-size: 18px;
  width: 100%;
  height: 100%;
  text-align: center;
  text-decoration: none;
}
.Navbar > .links > a:first-child {
  border-right: 1px solid transparent;
}
.Navbar > .links > a:last-child {
  border-left: 1px solid transparent;
}
.Navbar > .links > a:hover {
  background-color: #eee;
  color: #0ba8ff;
  border-color: #d5d5d5;
}
.Navbar > div {
  padding: 10px;
  width: 100%;
  font-weight: bold;
}
.Navbar > div > .blue {
  padding-left: 5px;
  font-weight: bold;
  color: #0ba8ff;
}
.Navbar > .version {
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-direction: row;
  width: 100%;
}
.Navbar > .version > .menu {
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin-left: auto;
  line-height: 0.4;
  border: 1px solid #bbbbbb;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  width: 60px;
  height: 35px;
  border-radius: 10px;
  transition: all 0.1s ease-in;
  font-weight: bolder;
  font-size: 20px;
  overflow: hidden;
}
.Navbar > .version > .menu:hover {
  color: #0ba8ff;
  background-color: #d5d5d5;
}
@media only screen and (min-width: 1100px) {
  .Navbar > .version > .menu {
    display: none;
  }
}
.Navbar > .menu {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  flex-wrap: nowrap;
  padding: 0;
  margin: 0;
  width: 100%;
  transition: all 0.1s ease-in-out;
  height: auto;
}
@media only screen and (max-width: 1100px) {
  .Navbar > .menu {
    overflow: auto;
  }
  .Navbar > .menu.-show {
    max-height: 500px;
  }
  .Navbar > .menu.-hide {
    max-height: 0;
    overflow: hidden;
    -webkit-transition: max-height 0.3s;
    -moz-transition: max-height 0.3s;
    transition: max-height 0.3s;
  }
}
.Navbar > .menu > a {
  text-decoration: none;
  color: #333;
  width: 100%;
  white-space: nowrap;
  padding: 10px;
  transition: all 0.1s ease-in;
  border-top: 1px solid transparent;
  border-bottom: 1px solid transparent;
  font-weight: bold;
}
.Navbar > .menu > a:last-child {
  border-bottom: none;
}
.Navbar > .menu > a:hover {
  background-color: #eee;
  color: #0ba8ff;
  border-color: #d5d5d5;
}
.Navbar > .menu > .sub-list {
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;
  width: 100%;
}
.Navbar > .menu > .sub-list > a {
  text-decoration: none;
  color: #333;
  width: 100%;
  white-space: nowrap;
  padding: 10px;
  padding-left: 40px;
  transition: all 0.1s ease-in;
  border-top: 1px solid transparent;
  border-bottom: 1px solid transparent;
}
.Navbar > .menu > .sub-list > a:hover {
  background-color: #eee;
  color: #0ba8ff;
  border-color: #d5d5d5;
}